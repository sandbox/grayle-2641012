(function ($) {
    Drupal.behaviors.scald_flexslider_respsonsive_iframe = {
        attach: function (context, settings) {

            // We move the author and tag fields outside of the responsive wrapper
            // They have no business there
            // Result: same hierarchy as default
            // @todo see if we can't alter the render at some point to move the fields there in PHP
            $('.scald-flexslider-responsive-iframe-wrapper > div').each(function () {
                $(this).parent().after(this);
            });
        }
    };
}(jQuery));