<?php
/**
 * @file
 * Hooks provided by the Scald FlexSlider module
 *
 * As with all hooks, call order for a hook implementation is determined by module weight
 */

/**
 * Alter the render array for a single slide
 * Gets called before hook_SCALD_TYPE_scald_flexslider_slide_render_array_alter
 * Gets called before hook_SCALD_PROVIDER_scald_flexslider_slide_render_array_alter
 *
 * @param $render_array
 *    The render array for the slide
 * @param $attached
 *    Attach required JS, CSS and Libraries to this array
 *    Do not add them to the render array
 *    Do not call drupal_add_js
 *    Atom caching will not add the files on subsequent page loads if you do
 * @param $variables
 *    Array containing the gallery atom and gallery item atom
 *    - gallery_atom: the gallery atom
 *    - gallery_item: the gallery item atom
 *    - gallery_atom_context: the context of the gallery atom
 *    - gallery_item_context: the context of the gallery item atom
 *    - settings: the player settings
 */
function hook_scald_flexslider_slide_render_array_alter(&$render_array, &$attached, $variables) {

  $render_array['content'] = array(
    '#prefix' => '<div class="custom-flexslider-wrapper">',
    '#suffix' => '</div>',
    '#markup' => scald_render($variables['gallery_item'], $variables['gallery_item_context']),
  );

  $attached['#attached']['css'][] = 'path/to/css';
}


/**
 * Alter the render array for a single slide
 * Gets called before hook_SCALD_PROVIDER_scald_flexslider_slide_render_array_alter
 *
 * @param $render_array
 *    The render array for the slide
 * @param $attached
 *    Attach required JS, CSS and Libraries to this array
 *    Do not add them to the render array
 *    Do not call drupal_add_js
 *    Atom caching will not add the files on subsequent page loads if you do
 * @param $variables
 *    Array containing the gallery atom and gallery item atom
 *    - gallery_atom: the gallery atom
 *    - gallery_item: the gallery item atom
 *    - gallery_atom_context: the context of the gallery atom
 *    - gallery_item_context: the context of the gallery item atom
 *    - settings: the player settings
 */
function hook_SCALD_TYPE_scald_flexslider_slide_render_array_alter(&$render_array, &$attached, $variables) {

  // unset the default content, or you can just override it directly this is just
  // an example
  // The render array may still be an empty array.
  if (isset($render_array['content'])) {
    unset($render_array['content']);
  }

  $render_array['SCALD_TYPE_content'] = array(
    '#prefix' => '<div class="custom-flexslider-wrapper">',
    '#suffix' => '</div>',
    '#markup' => scald_render($variables['gallery_item'], $variables['gallery_item_context']),
  );
}

/**
 * Alter the render array for a single slide of a certain scald provider
 * Gets called last, as it is the most specific
 *
 * @param $render_array
 *    The render array for the slide
 * @param $attached
 *    Attach required JS, CSS and Libraries to this array
 *    Do not add them to the render array
 *    Do not call drupal_add_js
 *    Atom caching will not add the files on subsequent page loads if you do
 * @param $variables
 *    Array containing the gallery atom and gallery item atom
 *    - gallery_atom: the gallery atom
 *    - gallery_item: the gallery item atom
 *    - gallery_atom_context: the context of the gallery atom
 *    - gallery_item_context: the context of the gallery item atom
 *    - settings: the player settings
 */
function hook_SCALD_PROVIDER_scald_flexslider_slide_render_array_alter(&$render_array, &$attached, $variables) {

  // unset the default content, or you can just override it directly this is just
  // an example
  // The render array may still be an empty array.
  if (isset($render_array['content'])) {
    unset($render_array['content']);
  }

  if (isset($render_array['SCALD_TYPE_content'])) {
    unset($render_array['SCALD_TYPE_content']);
  }

  $render_array['SCALD_PROVIDER_content'] = array(
    '#prefix' => '<div class="custom-flexslider-wrapper">',
    '#suffix' => '</div>',
    '#markup' => scald_render($variables['gallery_item'], $variables['gallery_item_context']),
  );

  $attached['#attached']['library'][] = array('module', 'library');
}

/**
 * Alter the iframe options used in the player settings form
 * If you implement this, you also need to implement one of the render_array_alters
 *
 * @param $iframe_options
 *    options array. Used in options form element
 * @param $description
 *    descriptions array. Used below the form element in the description
 *    Key this using the same key as the $iframe_options and provide a short explanation
 */
function hook_scald_flexslider_iframe_handling_alter(&$iframe_options, &$description) {
  $iframe_options['custom_handling'] = t('Custom Handling');
  $description['custom_handling'] = '<strong>' . t('Custom Handling') . '</strong>: ' . t('This provides custom handling of iframe-based video atoms by doing XYZ');
}