CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers
 
 
INTRODUCTION
------------

The Scald FlexSlider module adds a player for the Scald Gallery module. It 
provides integration with FlexSlider through the FlexSlider drupal module.
    
 * For a full description of the module, visit the project page:
   https://drupal.org/sandbox/grayle/2641012
    
 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/2641012
     

REQUIREMENTS
------------

This module requires the following modules:

 * Scald Gallery (https://drupal.org/project/scald_gallery)
 
 * FlexSlider (https://drupal.org/project/flexslider)
 
 
RECOMMENDED MODULES
-------------------

 * jQuery Update (https://drupal.org/project/jquery_update)
   When enabled, allows setting of jQuery versions through the UI
   
 * Epsacrop (https://drupal.org/project/epsacrop)
   When enabled and configured, allows users to apply image crops to
   uploaded images
   
   
INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
   
   
CONFIGURATION
-------------

 * Configuring the FlexSlider player
 
    - Go to admin/structure/scald/gallery/contexts and select the context you
      wish to use the FlexSlider for. 
      
      Transcoder: Passthrough
      Player: FlexSlider
      
    - Click "Configure the player settings."
    
      Flexslider option set: select which FlexSlider option set you wish to use
      for this player instance. See the FlexSlider module on how to configure
      a FlexSlider optionset.
      
      Context: select which context you wish the gallery item atoms to use when
      rendering
      
      Iframe handling: select how you wish to handle iframe-based video atoms.
      By default only 'Default' is available. Enable the included modules to 
      enable more options
      
      Info handling: select how you wish to handle the scald authors and scald
      tags fields. 
        * Default: Do nothing
        * Hide: Hide them
        * Popup: Show a small link on the image that opens a CSS overlay with 
          the information when clicked
 
 * When dropping a gallery in a WYSIWYG:
 
    - Remember to right-click the atom and select the context you've configured
    
 * When using an atom reference field:
 
    - Remember to set the field formatter to the context you've configured. 
    
   
TROUBLESHOOTING
---------------

 * The slide does not display
 
    - Check the console for any jquery errors. If a method is undefined you 
      need to update your jQuery version.
      
    - Check the field formatter or atom context and make sure it's set correctly
    
    - Check that the context you've chosen to render the gallery item atoms 
      in is configured correctly (not set to Title)
      
  
 * Everything's freaking out!
 
    - Check that you're not including the same atom twice in the gallery. The
      IDs are based on the atom sid and this can break a lot of things if the 
      same ID is present twice in the HTML document.
     
    - Check you're not rendering the gallery twice (once in WYSIWYG, once in 
      an atom reference field for example) for the same reason as above.


MAINTAINERS
-----------

Current maintainers:
 * Nick Vanpraet (Grayle) - https://drupal.org/user/3145497