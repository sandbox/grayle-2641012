(function ($) {
    Drupal.behaviors.scald_flexslider = {
        attach: function (context, settings) {
            // flexslider-load removes width and height attributes from the first elements inside the li
            // but in the case of popup info or hide info the images and iframes are one level deeper
            // If they're deeper still (because of iframe handling) that module handles it
            $('.scald-flexslider-player-wrapper ul')
                .first()
                .find('li > .popup-info > *, li > .hide-info > *')
                .removeAttr('width')
                .removeAttr('height');
        }
    };
}(jQuery));