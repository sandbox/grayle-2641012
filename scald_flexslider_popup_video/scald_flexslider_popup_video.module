<?php

/**
 * Implements hook_library().
 */
function scald_flexslider_popup_video_library() {
  $path = drupal_get_path('module', 'scald_flexslider_popup_video');

  $libraries = array(
    'flexslider_popup_iframe' => array(
      'title' => 'Flexslider Popup Iframe',
      'version' => '1.0',
      'js' => array(
        '//cdn.rawgit.com/noelboss/featherlight/1.3.5/release/featherlight.min.js' => array(
          'type' => 'external',
        ),
        $path . '/assets/scald_flexslider_popup_iframe.js' => array(
          'type' => 'file',
        ),
      ),
      'css' => array(
        $path . '/assets/scald_flexslider_popup_iframe.css' => array(
          'type' => 'file',
          'media' => 'screen',
        ),
        '//cdn.rawgit.com/noelboss/featherlight/1.3.5/release/featherlight.min.css' => array(
          'type' => 'external'
        ),
      ),
    ),
  );

  return $libraries;
}


/**
 * Implements hook_scald_flexslider_iframe_handling_alter().
 */
function scald_flexslider_popup_video_scald_flexslider_iframe_handling_alter(&$iframe_options, &$description) {
  $iframe_options['popup'] = t('Popup');
  $description['popup'] = '<strong>' . t('Popup') . '</strong>: ' . t('Uses Featherlight plugin to show the thumbnail in the slider and the video when the user clicks the thumbnail. Image style for the thumbnail is the same as those for normal image atoms in the slider.');
}


/**
 * Implements hook_SCALD_TYPE_scald_flexslider_slide_render_array_alter().
 */
function scald_flexslider_popup_video_video_scald_flexslider_slide_render_array_alter(&$render_array, &$attached, $variables) {
  // If the player isn't set to popup iframe handling, abort
  if ($variables['settings']['iframe_handling'] !== 'popup') {
    return;
  }

  // Only supports iframe based video atoms
  if (!in_array($variables['gallery_item']->provider, array('scald_youtube', 'scald_vimeo', 'scald_dailymotion'))) {
    return;
  }

  $render_array['content']['atom'] = scald_flexslider_popup_video_iframe_build($variables['gallery_item'], $variables['gallery_item_context']);
  //add libs
  $attached['#attached']['library']['flexslider_popup_iframe'] = array(
    'scald_flexslider_popup_video',
    'flexslider_popup_iframe'
  );

  // We don't need any special info handling, the js file moving the author and tag fields outside of the wrapper
  // means the parent rendering and styling suffices.
}


/**
 * Build render array for a popup iframe slider
 * Does not attach needed libraries
 *
 * @param $atom
 * @param $context
 * @return array
 */
function scald_flexslider_popup_video_iframe_build($atom, $context) {
  $image_atom = clone $atom;
  $image_atom->provider = 'image';
  $image_atom->type = 'image';
  // don't call scald_render, even with rebuild => true here and in the final render
  // it, for some reason, adds the link to every part of the atom. The image, the tags, the authors.
  scald_prerender($image_atom, $context, array());
  scald_image_scald_prerender($image_atom, $context, array(), 'atom');
  $slide_image = $image_atom->rendered->player;

  //ID featherlight needs
  $featherlight_trigger_id = 'scald-flexslider-featherlight-id-' . $atom->sid;

  //build the render array
  $render = array(
    "link" => array(
      '#markup' => l($slide_image, '#', array(
        'html' => TRUE,
        'attributes' => array(
          'data-featherlight' => '#' . $featherlight_trigger_id,
          'class' => array('scald-flexslider-featherlight-trigger', 'popup-iframe')
        )
      )),
    ),
    'featherlight-content' => array(
      '#prefix' => '<div id="' . $featherlight_trigger_id . '" class="scald-flexslider-featherlight-lightbox">',
      '#markup' => scald_render($atom, $context),
      '#suffix' => '</div>',
    ),
  );
  return $render;
}