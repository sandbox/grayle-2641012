(function ($) {
    Drupal.behaviors.scald_flexslider_popup_iframe = {
        attach: function (context, settings) {
            // flexslider-load removes width and height attributes from the first elements inside the li
            // but for this player they're one level deeper by default (inside an a tag)
            $('.scald-flexslider-player-wrapper .scald-flexslider-featherlight-trigger').removeAttr('width').removeAttr('height');

            // We move the author and tag fields outside of the featherlight content box
            // They have no business there
            // Result: same hierarchy as default
            $('.scald-flexslider-player-wrapper .scald-flexslider-featherlight-lightbox > div').each(function () {
                $(this).parent().after(this);
            });
        }
    };
}(jQuery));